sudo du --max-depth=1 /disk/users | sort -n -r | awk -F " " '{print $2, int(int($1)/(1024*1024)), "GB"}'
