#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Run like this: ./add_user.sh sid"
    exit
fi

SID=$1

read -p "LDAP Manager Password: " -s LDAPPASS
echo

HIGHEST_UID=$(sudo ldapsearch -x -w "$LDAPPASS" -b "ou=People,dc=ar2a" -D "cn=ldapadm,dc=ar2a" "(objectclass=posixaccount)" uidnumber | grep -e '^uid' | cut -d':' -f2 | sort | tail -1)

if [ -z $HIGHEST_UID ]
then
  USER_ID=1001
else
  USER_ID=$((${HIGHEST_UID}+1))
fi

CHANGE_DATE=$(echo "$(date +%s) / ( 60 * 60 * 24 )" | bc)

LDIF=$(cat<<EOF
dn: uid=${SID},ou=People,dc=ar2a
objectClass: top
objectClass: account
objectClass: posixAccount
objectClass: shadowAccount
cn: ${SID}
uid: ${SID}
uidNumber: ${USER_ID}
gidNumber: 1000
homeDirectory: /disk/users/${SID}
loginShell: /bin/bash
userPassword: ${SID}1
gecos: Comment
shadowLastChange: ${CHANGE_DATE}
shadowMin: 0
shadowMax: 99999
shadowWarning: 7
EOF
)

echo "--------------------"
echo "Adding ${LDIF}"
echo "--------------------"
echo "$LDIF" | ldapadd -x -w "$LDAPPASS" -D "cn=ldapadm,dc=ar2a"

unset LDAPPASS
