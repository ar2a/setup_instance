#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Run like this: ./add_user sid"
    exit
fi

USER=$1
PASSWORD=$11
GROUP=ar2a

echo "Creating account for: $USER"
echo "Default password: $PASSWORD"
echo "Default group: $GROUP"

sudo useradd -m $USER -g $GROUP
echo $USER:$PASSWORD | sudo chpasswd

if [ ! -d /disk/user ]; then
  mkdir -p /disk/users;
fi

OLD_HOME=/home/$USER
NEW_HOME=/disk/users/$USER

sudo mkdir $NEW_HOME

sudo cp -rT $OLD_HOME $NEW_HOME
sudo rm -rf $OLD_HOME
sudo ln -s $NEW_HOME $OLD_HOME

sudo cp .bashrc $NEW_HOME
sudo cp .condarc $NEW_HOME

sudo chown -h $USER:$GROUP $OLD_HOME
sudo chown -R $USER:$GROUP $NEW_HOME

