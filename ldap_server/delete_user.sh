#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Run like this: ./delete_user sid"
    exit
fi

SID=$1

sudo ldapdelete -W -D "cn=ldapadm,dc=ar2a" "uid=${SID},ou=People,dc=ar2a"
